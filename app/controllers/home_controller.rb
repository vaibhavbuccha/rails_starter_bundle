class HomeController < ApplicationController
  def index
  	if current_user.present?
  		if current_user.role == 1
  			redirect_to admin_dashboard_index_path
  		end
  	end
  end
end
