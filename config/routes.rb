Rails.application.routes.draw do
  get 'products/index'
  # devise_for :users
  root 'home#index'
  devise_for :users, controllers: {
        sessions: 'users/sessions'
  }

  namespace :admin do
	  get 'dashboard/index'
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
